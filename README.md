# [*Kaggle Data Challenge - Kernel Methods*](http://lear.inrialpes.fr/people/mairal/teaching/2015-2016/MVA/) (Master MVA)
### Meta-data:
- *Subject*: Classifing hand-written digits with Kernel Methods,
- *Advisor*: [Julien Mairal](http://lear.inrialpes.fr/people/mairal/) (Inria Grenoble),
- *Keywords*: Kernel Methods, SVM, MNIST, Kaggle,
- *Where*: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva16-kaggle-data-challenge-kernel-methods),
- *Who*: [our team](https://www.kaggle.com/t/282136/l-besson-m-ralle-j-roquero), [Lilian Besson](https://www.kaggle.com/naereen), [Jaime Roquiro](https://www.kaggle.com/XXX) and [Mathilde Ralle](https://www.kaggle.com/meldimath),
- [On *Kaggle InClass*](https://inclass.kaggle.com/c/kernel-methods-in-machine-learning-mva/).

### Task:
- We got 5000 training data, 8x8 pixels gray-scaled images of hand-written digits, along with their label ("which digit ?");
- We also got 10000 testing data, with the same format, but an unknown label;
- And the task was obviously to predict the labels on the testing data by using the training data (*supervised* learning).

### Feedback:
- **Grade**: we got **14.6/20** (*9/10* for the report, *5.98/10* for the Kaggle score);
- **Kaggle Rank**: our final rank on [the leaderboard for this Kaggle Data Challenge](https://inclass.kaggle.com/c/kernel-methods-in-machine-learning-mva/leaderboard) was 46/71 (71 teams), with a score of **94.52%**.
- [I (Lilian Besson)](http://perso.crans.org/besson/) got **17.3/20** for the whole course. Personal **Rank**: 20 amongst *151* students (average was *14.7/20*);

----

## Research report (DONE)
### [Final research report](./report/)
> - 2 pages. It is due March 22th (2016),

See [here for the (final) PDF report](https://bitbucket.org/lbesson/mva16-kaggle-data-challenge-kernel-methods/downloads/MVA_Kaggle_Data_Challenge__Kernel_Methods__J_Roquiro__L_Besson__M_Ralle__2015-16__Report.en.pdf) (*not yet*).

----

## TODO list (DONE)
### Main dates
- [X] Before 1st of March: create git, report of OverLeaf, first script etc,
- [X] 25th of February: opening of the Kaggle Data Challenge,
- [X] 14th of March: last day of submission,
- [X] 22th of March: hard deadline for the programs and report.

### [Code](./src/) (in [Python](https://www.python.org/)) (DONE)
> See [the README about code](./src/README.md), and [the requirements](./src/requirements.txt).

- [X] Explore the possible Kernel-powered models (with [scikit-learn](http://scikit-learn.org/)),
- [X] Find the best one / more probably the best one,
- [X] Tune his parameters,
- [X] Then, implement all this ourself.

### [Report](./report/) (DONE)
- [X] Explain our initial approach,
- [X] What we explored,
- [X] Using [Cross-validation](http://scikit-learn.org/stable/modules/cross_validation.html#cross-validation) or [grid-search](http://scikit-learn.org/stable/modules/grid_search.html#grid-search) (or intuition) to tune our model parameters.

----

## About
This project was done for the [*Kernel Methods*](http://lear.inrialpes.fr/people/mairal/teaching/2015-2016/MVA/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

> [Contributors](./contributors.txt) | [A few git statistics](./complete-stats.txt) | [Git Makefile](./Makefile)

### Copyright
(C), 2016, [Lilian Besson](http://perso.crans.org/besson/) ([ENS de Cachan](http://www.ens-cachan.fr)), [Jaime Roquiro]() ([ENS Ulm](http://www.ens.fr)) and [Mathilde Ralle]() ([Univ. Paris-Sud](http://www.u-psud.fr/)).

### Licence
This project was publicly published the 22-03-16, under the terms of the [MIT license](http://lbesson.mit-license.org/).
