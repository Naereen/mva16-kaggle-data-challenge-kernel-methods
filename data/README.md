## Data
- You will find here the [training](train.csv) and [testing](test.csv) datasets, as simple CSV files.
- But they are very heavy, so go on the [Kaggle Data Challenge data page](https://inclass.kaggle.com/c/kernel-methods-in-machine-learning-mva/data) and download them from there instead!

----

#### More ?
- See [the report](../report/) for more information.
- See [the code](../src/), [the figures](../fig/) or [the references](../biblio/) if needed.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
