## Figures
- These figures have been generated with [Python](https://www.python.org/), with [these programs](../src/),
- The figures are used in [the report](../report/).

----

#### More ?
- See [the report](../report/) for more information explanations about these figures.
- See [the code](../src/), [the figures](../fig/) or [the references](../biblio/) if needed.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
