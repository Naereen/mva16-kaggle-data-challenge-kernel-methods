#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" Manual implementation of the most useful kernels to use with our SVM classifier.

Every kernel is a function K: (X, Y) -> K(X, Y), it must take as arguments two matrices of shape (n_samples_1, n_features), (n_samples_2, n_features) and return a kernel matrix of shape (n_samples_1, n_samples_2).
Reference: http://scikit-learn.org/dev/modules/svm.html#kernel-functions.

DONE:
  - Linear kernel (X . Y)
  - RBF kernel exp(- gamma * |X - Y|^2). WARNING it is gamma * |X - Y|^2, NOT |X - Y|^2 / sigma^2
  - Polynomial kernel, with any degree d ((X . Y + r) ^ d)
  - Sigmoid kernel (tanh(gamma * (X . Y + r))
  - Laplace kernel exp(- gamma * |X - Y|)
  - User defined gamma, or auto selection of gamma (gamma = 'auto' ==> gamma = 1 / n_features)
  - Test all these kernels

NOT DONE:
  - More data-specific kernels


Examples, first with the RBF kernel:

>>> X = np.array([0, 1, 2, 3])
>>> Y = np.array([1, 1, 2, 2])
>>> K = rbf(gamma = 1)
>>> K(X, Y)
0.13533528323661262

Laplace and sigmoid kernels:

>>> K = laplace(gamma = 1)
>>> K(X, Y)
0.24311673443421419
>>> K = sigmoid(gamma = 0.05)
>>> K(X, Y)
0.50052021119023526

Linear kernel:

>>> K = linear()
>>> K(X, Y)
11

Polynomial kernels:

>>> K = poly(degree = 1, coef0 = 0)
>>> K(X, Y)
11
>>> K = poly(degree = 2, coef0 = 1)
>>> K(X, Y)
144
>>> K = poly(degree = 3)
>>> K(X, Y)
1728


- *Pylint:* Your code has been rated at 10.00/10.
- *Date:* Tuesday 15 February 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np
from numpy.linalg import norm


def _rbf(X, Y, gamma='auto'):
    """ RBF t.i. kernel (radial base function), K(X, Y) = exp(- gamma * |X - Y|^2)

    - Give gamma = 'auto' or gamma = 0 to select gamma as 1 / n_features.
    """
    if gamma == 'auto' or gamma == 0:
        n_features = np.shape(X)[1] if len(np.shape(X)) > 1 else 1
        gamma = 1 / n_features
    assert gamma > 0, "[ERROR] kernels.rbf: using a gamma < 0 will not do what you want."
    return np.exp(- gamma * norm(X - Y)**2)


# Done: make a rbf function taking gamma, returning the kernel (quicker for intensive computation)
def rbf(gamma='auto'):
    """ Return the RBF kernel (radial base function), K(X, Y) = exp(- gamma * |X - Y|^2).
    """
    if not isinstance(gamma, str) and gamma > 0:
        def kernel(X, Y):
            """ Parameter-free version of the RBF kernel (radial base function), K(X, Y) = exp(- gamma * |X - Y|^2).
            """
            return np.exp(- gamma * norm(X - Y)**2)
        return kernel
    else:
        return _rbf


def _laplace(X, Y, gamma='auto'):
    """ Laplace t.i. kernel, K(X, Y) = exp(- |X - Y| / gamma).

    - Give gamma = 'auto' or gamma = 0 to select gamma as 1 / n_features.
    """
    if gamma == 'auto' or gamma == 0:
        n_features = np.shape(X)[1] if len(np.shape(X)) > 1 else 1
        gamma = 1 / n_features
    assert gamma > 0, "[ERROR] kernels.laplace: using a gamma < 0 will not do what you want."
    return np.exp(- gamma * norm(X - Y))


# Done: make a laplace function taking gamma, returning the kernel (quicker for intensive computation)
def laplace(gamma='auto'):
    """ Return the Laplace kernel, K(X, Y) = exp(- |X - Y| / gamma).
    """
    if not isinstance(gamma, str) and gamma > 0:
        def kernel(X, Y):
            """ Parameter-free version of the Laplace kernel, K(X, Y) = exp(- |X - Y| / gamma).
            """
            return np.exp(- gamma * norm(X - Y))
        return kernel
    else:
        return _laplace


def _sigmoid(X, Y, gamma='auto', coef0=0):
    """ Sigmoid kernel, K(X, Y) = tanh(gamma (X . Y + r)

    - Give gamma = 'auto' or gamma = 0 to select gamma as 1 / n_features.
    - This kernel might not be as useless as it seems.
    - Ref: https://www.csie.ntu.edu.tw/~cjlin/papers/tanh.pdf
    """
    if gamma == 'auto' or gamma == 0:
        n_features = np.shape(X)[1] if len(np.shape(X)) > 1 else 1
        gamma = 1 / n_features
    assert gamma > 0, "[ERROR] kernels.sigmoid: using a gamma < 0 will not do what you want."
    return np.tanh(gamma * np.dot(X, Y) + coef0)


# Done: make a sigmoid function taking gamma, returning the kernel (quicker for intensive computation)
def sigmoid(gamma='auto', coef0=0):
    """ Return the sigmoid kernel, K(X, Y) = tanh(gamma (X . Y + r).
    """
    if not isinstance(gamma, str) and gamma > 0:
        def kernel(X, Y):
            """ Parameter-free version of the sigmoid, K(X, Y) = tanh(gamma (X . Y + r).
            """
            return np.tanh(gamma * np.dot(X, Y) + coef0)
        return kernel
    else:
        return _sigmoid


def _linear(X, Y):
    """ Linear kernel, simply the dot product K(X, Y) = (X . Y).
    """
    # return np.dot(X, Y)  # Would work too
    return np.inner(X, Y)


def linear():
    """ Linear kernel, simply the dot product K(X, Y) = (X . Y).
    """
    return _linear


def _poly(X, Y, degree=3, coef0=1):
    """ Parametrized version of the polynomial kernel, K(X, Y) = (X . Y + coef0)^degree.

    - Default coef0 is 1.
    - Default degree is 3. Computation time is CONSTANT with d, but that's not a reason to try huge values. degree = 2,3,4,5 should be enough.
    - Using degree = 1 is giving a (possibly) non-homogeneous linear kernel.
    """
    assert degree > 0, "[ERROR] kernels.poly: using a degree < 0 will fail (the kernel is not p.d.)."
    return (np.dot(X, Y) + coef0) ** degree


# Done: make a poly function taking degree,coef0 returning the kernel (quicker for intensive computation)
def poly(degree=3, coef0=1):
    """ Return the polynomial kernel of degree d (X, Y -> K(X, Y) = (X . Y + coef0)^d), degree = 3 by default.
    """
    assert degree > 0, "[ERROR] kernels.poly: using a degree < 0 will fail (the kernel is not p.d.)."

    def kernel(X, Y):
        """ Parameter-free version of the polynomial kernel, K(X, Y) = (X . Y + coef0)^d.
        """
        return (np.dot(X, Y) + coef0) ** degree
    return kernel

# End of kernels.py
