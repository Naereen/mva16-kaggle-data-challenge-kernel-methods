# Report

1272 statements analysed.

## Statistics by type

type | number | old number | difference | %documented | %badname  
---|---|---|---|---|---  
module | 8 | 8 | = | 100.00 | 12.50  
class | 3 | 3 | = | 100.00 | 66.67  
method | 26 | 26 | = | 100.00 | 0.00  
function | 56 | 56 | = | 100.00 | 19.64  
  
## External dependencies

    
    
    SMO (svm)
    cvxopt (svm)
      \-solvers (svm)
    joblib (svm)
    kernels (svm)
    matplotlib 
      \-pyplot (start,plot_digit)
    numba (svm)
    numpy (kernels,svm,test_svm_2D_data,SMO,start,scale,pca)
      \-linalg (kernels,pca,svm)
    pca (start)
    plot_digit (start)
    pylab (test_svm_2D_data)
    scale (start)
    scipy 
      \-linalg (pca)
    skimage 
      \-exposure (start)
      \-feature (start)
      \-filters (start)
    sklearn 
      \-cross_validation (start)
      \-ensemble (start)
      \-grid_search (start)
      \-metrics (start)
    svm (start,test_svm_2D_data)

## Raw metrics

type | number | % | previous | difference  
---|---|---|---|---  
code | 1344 | 55.17 | 1344 | =  
docstring | 534 | 21.92 | 534 | =  
comment | 251 | 10.30 | 251 | =  
empty | 307 | 12.60 | 307 | =  
  
## Duplication

  | now | previous | difference  
---|---|---|---  
nb duplicated lines | 48 | 48 | =  
percent duplicated lines | 1.858 | 1.858 | =  
  
## Messages by category

type | number | previous | difference  
---|---|---|---  
convention | 0 | 0 | =  
refactor | 20 | 20 | =  
warning | 24 | 24 | =  
error | 3 | 3 | =  
  
## % errors / warnings by module

module | error | warning | refactor | convention  
---|---|---|---|---  
svm | 100.00 | 62.50 | 15.00 | 0.00  
test_svm_2D_data | 0.00 | 33.33 | 70.00 | 0.00  
plot_digit | 0.00 | 4.17 | 0.00 | 0.00  
SMO | 0.00 | 0.00 | 15.00 | 0.00  
  
## Messages

message id | occurrences  
---|---  
attribute-defined-outside-init | 12  
too-many-locals | 11  
global-statement | 5  
duplicate-code | 5  
unexpected-keyword-arg | 3  
unbalanced-tuple-unpacking | 3  
too-many-branches | 2  
arguments-differ | 2  
unused-import | 1  
too-many-statements | 1  
too-many-instance-attributes | 1  
broad-except | 1  
  
## Global evaluation

Your code has been rated at 9.54/10 (previous run: 9.54/10, +0.00) So close to being perfect...
