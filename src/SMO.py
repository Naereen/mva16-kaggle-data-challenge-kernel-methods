#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" Experimental implementation of a more efficient algorithm to train a BinarySVC model.

DONE:
  - Implement a less naive way to learn the SVC (with the SMO algorithm)

Spoiler:
  - It works, but is EXTREMELY slow compared to cvxopt.solvers.qp.


References:
  - This short note on SMO, by Andrew Ng for the CS229 course at Stanford (2015): http://cs229.stanford.edu/materials/smo.pdf
  - And the lecture note on SVM:  http://cs229.stanford.edu/notes/cs229-notes3.pdf


Articles of reference:
  - https://en.wikipedia.org/wiki/Sequential_minimal_optimization (not clear enough);
  - Platt, John (1998), Sequential Minimal Optimization: A Fast Algorithm for Training Support Vector Machines, DOI: 10.1.1.43.4376 (http://www.msr-waypoint.com/pubs/69644/tr-98-14.pdf);
  - Platt, J. C. (1999). Using analytic QP and sparseness to speed training of support vector machines. Advances in neural information processing systems, 557-563 (http://ar.newsmth.net/att/148aa490aed5b5/smo-nips.pdf).


- *Pylint:* Your code has been rated at 9.19/10.
- *Date:* Sunday 13 March 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

# ----------------------------------------------------------------------------
# %% Initialization
from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np

# Tolerance epsilon for constraints
TOLERANCE = 1e-3

# Max number of passes in SMO without changing Lagrange multipliers alpha
MAX_PASSES = 3

# Max total number of passes in SMO
MAX_TOTAL_PASSES = 5e5


# ----------------------------------------------------------------------------
# %% Utility functions


def clip(a, L, H):
    """ Clip the value a to the interval [L, H], ensure the result is L <= a' <= H. """
    return max(L, min(a, H))


def isAtBound(a, C, tol=TOLERANCE):  # Useless
    """ Check that a = 0 or a = C, with a certain tolerance. """
    return (a <= tol) or (a >= C - tol)


def isNotAtBound(a, C):
    """ Check that a = 0 or a = C, without any tolerance. """
    return 0 < a < C


def isNotAtBoundTol(a, C, tol=TOLERANCE):  # Useless
    """ Check that a = 0 or a = C, with a certain tolerance. """
    return tol < a < C - tol


def roundBound(a, C, tol=TOLERANCE):
    """ Round a to 0 or to C, with a certain tolerance. """
    if a <= tol:
        a = 0
    elif a >= C - tol:
        a = C
    return a


def imply(A, B):  # Useless
    """ A ==> B  <---> B or (not A). """
    return B or (not A)


def violate_KKT(alphai, yi, Ei, C, tol=TOLERANCE):
    """ Check if the i-th KKT condition is violated, with a certain tolerance.

    - Reference: (6) in Platt, J. C. (1999). Using analytic QP and sparseness to speed training of support vector machines. Advances in neural information processing systems, 557-563 (http://ar.newsmth.net/att/148aa490aed5b5/smo-nips.pdf)
    """
    pred_i = yi * Ei
    # FIXED Here is how we should use the tolerance tol :
    check = ((pred_i < - tol and alphai < C) or (pred_i > tol and alphai > 0))
    # 1. Exact check :
    # check = imply(alphai == 0, pred_i >= 1) \
    #     and imply(0 < alphai < C, pred_i == 1) \
    #     and imply(alphai == C, pred_i <= 1)
    # 2. Approximated check :
    # check = imply(alphai < tol, pred_i >= 1 + tol) \
    #     and imply(tol < alphai < C - tol, 1 - tol <= pred_i <= 1 + tol) \
    #     and imply(alphai >= C - tol, pred_i <= 1 - tol)
    return check


def heuristicChoosej(n_samples, i):
    """ Heuristic to choose the index j for next solve2alpha call (when i has been chosen).

    - XXX implement a better heuristic choice?
    """
    j = np.random.choice([k for k in range(n_samples) if k != i], size=1, replace=False)[0]
    return j


def predictor(kernel, alpha, b, X, y, x):
    """ Predictor function, (2) in the reference document http://cs229.stanford.edu/materials/smo.pdf:

    f(x) = b + sum_{i=0}^{m-1} alpha_i . y_i . K( x_i, x )

    - XXX be sure it works! I think it's OK
    - FIXED use self.project instead ? Nope, self.a, self.sv, self.sv_y are not yet defined. And they are defined based on these alpha[i].
    """
    n_samples = np.shape(X)[0]
    return b + sum(alpha[i] * y[i] * kernel(X[i], x) for i in range(n_samples))  # OK use K(.,.)
    # return b + sum(alpha[i] * y[i] * np.dot(X[i], x) for i in range(n_samples))  # OK use <.,.>
    # return b + sum(alphai * yi * np.dot(Xi, x) for (alphai, yi, Xi) in zip(alpha, y, X))


# ----------------------------------------------------------------------------
# %% Main function

def smo(clf, X, y, K, tol=TOLERANCE, max_passes=MAX_PASSES, max_total_passes=MAX_TOTAL_PASSES):
    """ Train a BinarySVC model with the SMO algorithm and not a QP solver.

    Advantages:
      - Should be more efficient, both in term of memory and computation time
      - Should therefore allow to scale to "big data", or at least 5000 train points

    Arguments:
      - clf: a BinarySVC object, not trained yet
      - X: (n_samples, n_features) shaped array-like
      - y: (n_samples, 1) or (n_samples,) shaped array-like, labels in {-1, +1}
      - K: the precomputed Gram matrix K
      - tol: a small positive tolerance, controlling when a KKT condition is seen as satisfied or not
      - max_passes: number of times to iterate over alphas without changing
      - max_total_passes: max number of total passes

    Algorithm:
      1. Find a Lagrange multiplier alpha_i that violates the Karush-Kuhn-Tucker (KKT) conditions for the optimization problem.
      2. Pick a second multiplier alpha_j and optimize the pair (alpha_i,alpha_j).
      3. Repeat steps 1 and 2 until convergence.

    Questions:
      - Does it work directly for multi-class classification ? Nope, only for binary classification.
      - Does it requires optimization (either from cvxopt or scipy.optimize) ? Nope, just iterative loops like this.
    """
    log = clf._log
    logv = clf._logverbose
    logvv = clf._logverbverbose
    log("  Training BinarySVC with SMO.fit ... on X of shape {} ...".format(np.shape(X)))
    n_samples, _ = np.shape(X)
    C = clf._c
    log("  Using clf._c = C =", C)
    print("FIXME: what happen if C <= 0 ? or C is None ? Put C = 1e5 (~= +oo)")
    if C is None:
        C = 1e5
    # Now solve the QP problem with SMO algorithm. Cf. http://cs229.stanford.edu/materials/smo.pdf
    # Initialization
    alpha = np.zeros(n_samples)
    # alpha = np.ones(n_samples) * (C/2.0)  # Non-zero initial weights to speed up ?
    b = 0.0
    passes = 0
    total_passes = 0
    # Big loop!
    log("Starting big loop on #passes, with max_passes = {} and max_total_passes = {} ...".format(max_passes, max_total_passes))
    while (passes < max_passes) and (total_passes < max_total_passes):
        num_changed_alphas = 0
        for i in range(n_samples):
            total_passes += 1
            ui = predictor(clf._kernel, alpha, b, X, y, X[i])  # (2)
            yi = y[i]
            Ei = ui - yi  # (13)
            logvv("      > ui = {}, yi = {}, Ei = {} ...".format(ui, yi, Ei))
            if not violate_KKT(alpha[i], yi, Ei, C, tol=TOLERANCE):
                continue
            logv("  For i =", i, "the KKT condition is violated, working with this i, and Ei =", Ei, "...")
            # If i violates the KKT condition, find next j
            j = heuristicChoosej(n_samples, i)
            uj = predictor(clf._kernel, alpha, b, X, y, X[j])  # (2)
            yj = y[j]
            Ej = uj - yj  # (13)
            # logvv("      > uj = {}, yj = {}, Ej = {} ...".format(uj, yj, Ej))
            logv("    Random choice j =", j, "and Ej =", Ej)
            # Backup a_i a_j values
            a_i_old = alpha[i]
            a_j_old = alpha[j]
            # Compute the box constraints
            s = yi * yj  # Sign of difference of prediction, can be +1 if y1=y2, -1 otherwise
            L = max(0.0, a_j_old + s * a_i_old - (s + 1) * C / 2.0)  # (10,11)
            H = min(C, a_j_old + s * a_i_old - (s - 1) * C / 2.0)  # (10,11)
            logv("    Box constraints: L =", L, "and H =", H)
            if L == H:
                logv("    L = H, continue to next i ...")
                continue
            elif L > H:
                raise ValueError("SMO.fit: L > H should NEVER happen.")
            # Fetch the value of the kernel, only once
            Kii, Kjj, Kij = K[i, i], K[j, j], K[i, j]
            logv("    Using the Gram matrix: Kii =", Kii, "Kjj =", Kjj, "Kij =", Kij, "...")
            eta = 2 * Kij - Kii - Kjj  # (14)
            logv("    Computed eta =", eta, "...")
            if eta >= 0:  # FIXED no risk if eta == 0, to devide by eta for a_j_new
                log("    eta >= 0, continue to next i ...")
                continue
            # Update alpha[j], by compute (12), clip (15)
            # logvv("      > a_j_old =", a_j_old)
            a_j_new = a_j_old - yj * (Ei - Ej) / eta  # No risk of dividing by eta, cool
            # logvv("      > a_j_new =", a_j_new)
            a_j_new_clipped = clip(a_j_new, L, H)  # (15)
            # logvv("      > a_j_new_clipped =", a_j_new_clipped)
            a_j = roundBound(a_j_new_clipped, C, tol=tol)
            # logvv("      > a_j =", a_j)
            if abs(a_j - a_j_old) < tol:
                logv("    Too small change on alpha[j] =", a_j_old, "continue to next i ...")
                continue
            # Determine next value for alpha[i] by computing (16)
            # logvv("      > a_i_old =", a_i_old)
            a_i_new = a_i_old + s * (a_j_old - a_j)
            # logvv("      > a_i_new =", a_i_new)
            a_i_new_clipped = clip(a_i_new, L, H)  # (15)
            # logvv("      > a_i_new_clipped =", a_i_new_clipped)
            a_i = roundBound(a_i_new_clipped, C, tol=tol)
            # logvv("      > a_i =", a_i)
            # ATTENTION Modify in-place the Lagrange multipliers alpha
            logv("  ==>    Update alpha[i={}] = {:g} and alpha[j={}] = {:g} ...".format(i, a_i, j, a_j))
            alpha[i], alpha[j] = a_i, a_j
            # Update thresholds, using (17) and (18)
            b1 = b - Ei - yi * (a_i - a_i_old) * Kii - yj * (a_j - a_j_old) * Kij  # (17)
            b2 = b - Ej - yi * (a_i - a_i_old) * Kij - yj * (a_j - a_j_old) * Kjj  # (18)
            # Choose new threshold, by computing (19)
            if isNotAtBound(a_i, C):
                logv("   Updating b by b1 because 0 < alpha[i] < C ...")
                b = b1
            elif isNotAtBound(a_j, C):
                logv("   Updating b by b2 because 0 < alpha[j] < C ...")
                b = b2
            else:
                logv("   Updating b by (b1+b2)/2 because ...")
                b = (b1 + b2) / 2.0
            logv("  ==>    Update b =", b, "...")
            # Finally, we are still here if we changed something
            num_changed_alphas += 1
        # End for
        if num_changed_alphas == 0:
            passes += 1
            log("    + One more passes without changing any alphas! passes = {}. Total # = {} ...".format(passes, total_passes))
        else:
            log("    - At least one alpha got changed ({}), setting passes = 0. Total # = {}  ...".format(num_changed_alphas, total_passes))
            passes = 0
    # Done !
    return alpha, b, passes, total_passes


# End of SMO.py
