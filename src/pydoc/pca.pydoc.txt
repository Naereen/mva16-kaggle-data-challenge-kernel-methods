Help on module pca:

NAME
    pca - Manual implementation of the sklearn.decomposition.PCA class as myPCA.

FILE
    /home/lilian/ownCloud/cloud.openmailbox.org/Master_MVA_2015-16/projects/2nd-trimester/mva16-kaggle-data-challenge-kernel-methods/src/pca.py

DESCRIPTION
    - Implement a naive way to get the PCA (OK)
    - It imitates the interface (fit, fit_transform, transform) of sklearn.decomposition.PCA
    - Test it, by comparing to sklearn.decomposition.PCA (OK)
    - Work well, but is really slower than sklearn.decomposition.PCA
    
    Possible improvements:
      - If needed, implement a less naive way get the PCA (P-PCA ?)
      - If needed, implement Minka's strategy to automatically find a good n_components value
    
    References:
      - Mimicking the interface of the PCA class from scikit-learn: http://scikit-learn.org/stable/modules/decomposition.html#principal-component-analysis-pca, http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA
      - https://en.wikipedia.org/wiki/Principal_component_analysis
      - http://sebastianraschka.com/Articles/2014_pca_step_by_step.html
      - Minka, T. P. (2000, December). Automatic choice of dimensionality for PCA. In NIPS (Vol. 13, pp. 598-604).
    
    
    - *Pylint:* Your code has been rated at 10.00/10.
    - *Date:* Thursday 25 February 2016.
    - *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
    - *Licence:* MIT Licence (http://lbesson.mit-license.org).

CLASSES
    __builtin__.object
        myPCA
    
    class myPCA(__builtin__.object)
     |  My implementation of a PCA model. Use it like this: ::
     |  
     |      pca_model = myPCA(n_components=2)  # Initialize the model
     |      train = pca_model.fit_transform(train)  # Fit it and then transform the train data
     |      test = pca_model.transform(test)  # Transform the test data according to the learned transformation
     |  
     |  
     |  Linear dimensionality reduction using Singular Value Decomposition of the data and keeping only the most significant singular vectors to project the data to a lower dimensional space.
     |  
     |  This implementation uses the scipy.linalg implementation of the singular value decomposition.
     |  It only works for dense arrays and is not scalable to large dimensional data.
     |  
     |  Methods defined here:
     |  
     |  __init__(self, n_components=None, copy=False)
     |      Create a new PCA model, with n_components components.
     |      
     |      - n_components should be > 0,
     |      - Use a SVD decomposition (scipy.linalg.eigh), and a pretty naive algorithm,
     |      - Minka's MLE strategy to automatically find the best choice of n_components is NOT YET implemented,
     |      - copy is NOT used yet.
     |  
     |  __repr__ = __str__(self)
     |  
     |  __str__(self)
     |      str(self) -> str.
     |  
     |  fit(self, X, rho=False)
     |      Train the PCA model on data X..
     |      
     |      - X should not be too big (5000 samples, 1000 features max),
     |      - Every call to fit() will ERASE the memory of the previous fits,
     |      - if rho is True, print the ratio of the k-cumulated spectral energy by the total spectral energy (should be close to 100% if you want the PCA to be meaningful).
     |  
     |  fit_transform(self, X)
     |      Fit the PCA model on X, and then transform the data X (a numpy array-like) by the PCA transform.
     |  
     |  transform(self, X)
     |      Transform the data X (a numpy array-like) by the PCA transform.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)

DATA
    division = _Feature((2, 2, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0), 8192...
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


