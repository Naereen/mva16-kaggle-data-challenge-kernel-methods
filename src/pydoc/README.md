## Documentation
> Generated with [pydoc](https://docs.python.org/3/library/pydoc.html) command line program.

### For the programs:
- [for `kernels.py`](../kernels.py): documentation on [kernels.pydoc.txt](./kernels.pydoc.txt);
- [for `pca.py`](../pca.py): documentation on [pca.pydoc.txt](./pca.pydoc.txt);
- [for `plot_digit.py`](../plot_digit.py): documentation on [plot_digit.pydoc.txt](./plot_digit.pydoc.txt);
- [for `SMO.py`](../SMO.py): documentation on [SMO.pydoc.txt](./SMO.pydoc.txt);
- [for `svm.py`](../svm.py): documentation on [svm.pydoc.txt](./svm.pydoc.txt).

### For the script:
- [for `test_svm_2D_data.py`](../test_svm_2D_data.py): cf. its source code;
- [for `start.py`](../start.py): cf. its source code.
