Help on module SMO:

NAME
    SMO - Experimental implementation of a more efficient algorithm to train a BinarySVC model.

FILE
    /home/lilian/ownCloud/cloud.openmailbox.org/Master_MVA_2015-16/projects/2nd-trimester/mva16-kaggle-data-challenge-kernel-methods/src/SMO.py

DESCRIPTION
    DONE:
      - Implement a less naive way to learn the SVC (with the SMO algorithm)
    
    Spoiler:
      - It works, but is EXTREMELY slow compared to cvxopt.solvers.qp.
    
    
    References:
      - This short note on SMO, by Andrew Ng for the CS229 course at Stanford (2015): http://cs229.stanford.edu/materials/smo.pdf
      - And the lecture note on SVM:  http://cs229.stanford.edu/notes/cs229-notes3.pdf
    
    
    Articles of reference:
      - https://en.wikipedia.org/wiki/Sequential_minimal_optimization (not clear enough);
      - Platt, John (1998), Sequential Minimal Optimization: A Fast Algorithm for Training Support Vector Machines, DOI: 10.1.1.43.4376 (http://www.msr-waypoint.com/pubs/69644/tr-98-14.pdf);
      - Platt, J. C. (1999). Using analytic QP and sparseness to speed training of support vector machines. Advances in neural information processing systems, 557-563 (http://ar.newsmth.net/att/148aa490aed5b5/smo-nips.pdf).
    
    
    - *Pylint:* Your code has been rated at 9.19/10.
    - *Date:* Sunday 13 March 2016.
    - *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
    - *Licence:* MIT Licence (http://lbesson.mit-license.org).

FUNCTIONS
    clip(a, L, H)
        Clip the value a to the interval [L, H], ensure the result is L <= a' <= H.
    
    heuristicChoosej(n_samples, i)
        Heuristic to choose the index j for next solve2alpha call (when i has been chosen).
        
        - XXX implement a better heuristic choice?
    
    imply(A, B)
        A ==> B  <---> B or (not A).
    
    isAtBound(a, C, tol=0.001)
        Check that a = 0 or a = C, with a certain tolerance.
    
    isNotAtBound(a, C)
        Check that a = 0 or a = C, without any tolerance.
    
    isNotAtBoundTol(a, C, tol=0.001)
        Check that a = 0 or a = C, with a certain tolerance.
    
    predictor(kernel, alpha, b, X, y, x)
        Predictor function, (2) in the reference document http://cs229.stanford.edu/materials/smo.pdf:
        
        f(x) = b + sum_{i=0}^{m-1} alpha_i . y_i . K( x_i, x )
        
        - XXX be sure it works! I think it's OK
        - FIXED use self.project instead ? Nope, self.a, self.sv, self.sv_y are not yet defined. And they are defined based on these alpha[i].
    
    roundBound(a, C, tol=0.001)
        Round a to 0 or to C, with a certain tolerance.
    
    smo(clf, X, y, K, tol=0.001, max_passes=3, max_total_passes=500000.0)
        Train a BinarySVC model with the SMO algorithm and not a QP solver.
        
        Advantages:
          - Should be more efficient, both in term of memory and computation time
          - Should therefore allow to scale to "big data", or at least 5000 train points
        
        Arguments:
          - clf: a BinarySVC object, not trained yet
          - X: (n_samples, n_features) shaped array-like
          - y: (n_samples, 1) or (n_samples,) shaped array-like, labels in {-1, +1}
          - K: the precomputed Gram matrix K
          - tol: a small positive tolerance, controlling when a KKT condition is seen as satisfied or not
          - max_passes: number of times to iterate over alphas without changing
          - max_total_passes: max number of total passes
        
        Algorithm:
          1. Find a Lagrange multiplier alpha_i that violates the Karush-Kuhn-Tucker (KKT) conditions for the optimization problem.
          2. Pick a second multiplier alpha_j and optimize the pair (alpha_i,alpha_j).
          3. Repeat steps 1 and 2 until convergence.
        
        Questions:
          - Does it work directly for multi-class classification ? Nope, only for binary classification.
          - Does it requires optimization (either from cvxopt or scipy.optimize) ? Nope, just iterative loops like this.
    
    violate_KKT(alphai, yi, Ei, C, tol=0.001)
        Check if the i-th KKT condition is violated, with a certain tolerance.
        
        - Reference: (6) in Platt, J. C. (1999). Using analytic QP and sparseness to speed training of support vector machines. Advances in neural information processing systems, 557-563 (http://ar.newsmth.net/att/148aa490aed5b5/smo-nips.pdf)

DATA
    MAX_PASSES = 3
    MAX_TOTAL_PASSES = 500000.0
    TOLERANCE = 0.001
    division = _Feature((2, 2, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0), 8192...
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


