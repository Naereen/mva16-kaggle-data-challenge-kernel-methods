************* Module SMO
R:127, 0: Too many local variables (43/15) (too-many-locals)
R:127, 0: Too many branches (13/12) (too-many-branches)
R:127, 0: Too many statements (78/50) (too-many-statements)
************* Module plot_digit
W: 40,11: Catching too general exception Exception (broad-except)
************* Module svm
R:108, 0: Too many instance attributes (17/7) (too-many-instance-attributes)
E:174,31: Unexpected keyword argument 'coef0' in function call (unexpected-keyword-arg)
E:174,31: Unexpected keyword argument 'degree' in function call (unexpected-keyword-arg)
E:176,31: Unexpected keyword argument 'coef0' in function call (unexpected-keyword-arg)
R:207, 4: Too many branches (16/12) (too-many-branches)
R:287, 4: Too many local variables (18/15) (too-many-locals)
W:256, 8: Attribute 'a' defined outside __init__ (attribute-defined-outside-init)
W:269, 8: Attribute 'b' defined outside __init__ (attribute-defined-outside-init)
W:255, 8: Attribute '_ind' defined outside __init__ (attribute-defined-outside-init)
W:258, 8: Attribute 'sv_y' defined outside __init__ (attribute-defined-outside-init)
W:257, 8: Attribute 'sv' defined outside __init__ (attribute-defined-outside-init)
W:273,12: Attribute 'w' defined outside __init__ (attribute-defined-outside-init)
W:278,12: Attribute 'w' defined outside __init__ (attribute-defined-outside-init)
W:282, 8: Attribute 'n_support_' defined outside __init__ (attribute-defined-outside-init)
W:588, 4: Arguments number differs from overridden 'project' method (arguments-differ)
W:630, 4: Arguments number differs from overridden 'predict' method (arguments-differ)
W:547, 8: Attribute 'b' defined outside __init__ (attribute-defined-outside-init)
W:548, 8: Attribute 'w' defined outside __init__ (attribute-defined-outside-init)
W:542, 8: Attribute '_binary_SVCs' defined outside __init__ (attribute-defined-outside-init)
W:546, 8: Attribute 'n_support_' defined outside __init__ (attribute-defined-outside-init)
W: 66, 4: Unused jit imported from numba (unused-import)
************* Module test_svm_2D_data
R: 58, 0: Too many local variables (16/15) (too-many-locals)
R: 82, 0: Too many local variables (22/15) (too-many-locals)
R:149, 0: Too many local variables (16/15) (too-many-locals)
R:165, 0: Too many local variables (16/15) (too-many-locals)
R:181, 0: Too many local variables (28/15) (too-many-locals)
R:203, 0: Too many local variables (28/15) (too-many-locals)
W:266, 4: Possible unbalanced tuple unpacking with sequence: left side has 2 label(s), right side has 0 value(s) (unbalanced-tuple-unpacking)
W:298, 4: Possible unbalanced tuple unpacking with sequence: left side has 2 label(s), right side has 0 value(s) (unbalanced-tuple-unpacking)
R:310, 0: Too many local variables (17/15) (too-many-locals)
W:349, 4: Possible unbalanced tuple unpacking with sequence: left side has 2 label(s), right side has 0 value(s) (unbalanced-tuple-unpacking)
W:363, 4: Using the global statement (global-statement)
W:388, 4: Using the global statement (global-statement)
W:414, 4: Using the global statement (global-statement)
R:437, 0: Too many local variables (22/15) (too-many-locals)
W:439, 4: Using the global statement (global-statement)
R:474, 0: Too many local variables (28/15) (too-many-locals)
W:476, 4: Using the global statement (global-statement)
R:  1, 0: Similar lines in 2 files
==pca:20
==scale:9
- *Pylint:* Your code has been rated at 10.00/10.
- *Date:* Thursday 25 February 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np
 (duplicate-code)
R:  1, 0: Similar lines in 2 files
==SMO:24
==svm:24
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

# ----------------------------------------------------------------------------
# %% Initialization
from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np (duplicate-code)
R:  1, 0: Similar lines in 2 files
==start:29
==test_svm_2D_data:16
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
print("\nLaunching script, importing modules ...")
from time import time (duplicate-code)
R:  1, 0: Similar lines in 3 files
==pca:21
==plot_digit:8
==scale:10
- *Date:* Thursday 25 February 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed (duplicate-code)
R:  1, 0: Similar lines in 3 files
==kernels:58
==pca:22
==scale:11
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np (duplicate-code)


Report
======
1274 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |8      |8          |=          |100.00      |12.50    |
+---------+-------+-----------+-----------+------------+---------+
|class    |3      |3          |=          |100.00      |66.67    |
+---------+-------+-----------+-----------+------------+---------+
|method   |26     |26         |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|function |56     |56         |=          |100.00      |19.64    |
+---------+-------+-----------+-----------+------------+---------+



External dependencies
---------------------
::

    SMO (svm)
    cvxopt (svm)
      \-solvers (svm)
    joblib (svm)
    kernels (svm)
    matplotlib
      \-pyplot (start,plot_digit)
    numba (svm)
    numpy (kernels,svm,test_svm_2D_data,SMO,start,scale,pca)
      \-linalg (kernels,pca,svm)
    pca (start)
    plot_digit (start)
    pylab (test_svm_2D_data)
    scale (start)
    scipy
      \-linalg (pca)
    skimage
      \-exposure (start)
      \-feature (start)
      \-filters (start)
    sklearn
      \-cross_validation (start)
      \-ensemble (start)
      \-grid_search (start)
      \-metrics (start)
    svm (start,test_svm_2D_data)



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |1346   |55.25 |1346     |=          |
+----------+-------+------+---------+-----------+
|docstring |534    |21.92 |534      |=          |
+----------+-------+------+---------+-----------+
|comment   |251    |10.30 |251      |=          |
+----------+-------+------+---------+-----------+
|empty     |305    |12.52 |305      |=          |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |48    |48       |=          |
+-------------------------+------+---------+-----------+
|percent duplicated lines |1.858 |1.858    |=          |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |0      |0        |=          |
+-----------+-------+---------+-----------+
|refactor   |20     |20       |=          |
+-----------+-------+---------+-----------+
|warning    |24     |98       |-74.00     |
+-----------+-------+---------+-----------+
|error      |3      |3        |=          |
+-----------+-------+---------+-----------+



% errors / warnings by module
-----------------------------

+-----------------+-------+--------+---------+-----------+
|module           |error  |warning |refactor |convention |
+=================+=======+========+=========+===========+
|svm              |100.00 |62.50   |15.00    |0.00       |
+-----------------+-------+--------+---------+-----------+
|test_svm_2D_data |0.00   |33.33   |70.00    |0.00       |
+-----------------+-------+--------+---------+-----------+
|plot_digit       |0.00   |4.17    |0.00     |0.00       |
+-----------------+-------+--------+---------+-----------+
|SMO              |0.00   |0.00    |15.00    |0.00       |
+-----------------+-------+--------+---------+-----------+



Messages
--------

+-------------------------------+------------+
|message id                     |occurrences |
+===============================+============+
|attribute-defined-outside-init |12          |
+-------------------------------+------------+
|too-many-locals                |11          |
+-------------------------------+------------+
|global-statement               |5           |
+-------------------------------+------------+
|duplicate-code                 |5           |
+-------------------------------+------------+
|unexpected-keyword-arg         |3           |
+-------------------------------+------------+
|unbalanced-tuple-unpacking     |3           |
+-------------------------------+------------+
|too-many-branches              |2           |
+-------------------------------+------------+
|arguments-differ               |2           |
+-------------------------------+------------+
|unused-import                  |1           |
+-------------------------------+------------+
|too-many-statements            |1           |
+-------------------------------+------------+
|too-many-instance-attributes   |1           |
+-------------------------------+------------+
|broad-except                   |1           |
+-------------------------------+------------+



Global evaluation
-----------------
Your code has been rated at 9.54/10 (previous run: 8.96/10, +0.58)
So close to being perfect...

