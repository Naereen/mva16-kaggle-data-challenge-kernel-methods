#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" Manual implementation of the sklearn.decomposition.PCA class as myPCA.

- Implement a naive way to get the PCA (OK)
- It imitates the interface (fit, fit_transform, transform) of sklearn.decomposition.PCA
- Test it, by comparing to sklearn.decomposition.PCA (OK)
- Work well, but is really slower than sklearn.decomposition.PCA

Possible improvements:
  - If needed, implement a less naive way get the PCA (P-PCA ?)
  - If needed, implement Minka's strategy to automatically find a good n_components value

References:
  - Mimicking the interface of the PCA class from scikit-learn: http://scikit-learn.org/stable/modules/decomposition.html#principal-component-analysis-pca, http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA
  - https://en.wikipedia.org/wiki/Principal_component_analysis
  - http://sebastianraschka.com/Articles/2014_pca_step_by_step.html
  - Minka, T. P. (2000, December). Automatic choice of dimensionality for PCA. In NIPS (Vol. 13, pp. 598-604).


- *Pylint:* Your code has been rated at 10.00/10.
- *Date:* Thursday 25 February 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np

# First, we need a reference implementation of eigh:
try:
    from scipy.linalg import eigh
except ImportError:
    try:
        from numpy.linalg import eigh
    except ImportError:
        raise ImportError("Unable to find an implementation of the eigh function (neither numpy.linalg nor scipy.linalg was available.")

# If my implementation breaks down, use this one instead:
# from sklearn.decomposition import PCA as myPCA


class myPCA(object):
    """ My implementation of a PCA model. Use it like this: ::

        pca_model = myPCA(n_components=2)  # Initialize the model
        train = pca_model.fit_transform(train)  # Fit it and then transform the train data
        test = pca_model.transform(test)  # Transform the test data according to the learned transformation


    Linear dimensionality reduction using Singular Value Decomposition of the data and keeping only the most significant singular vectors to project the data to a lower dimensional space.

    This implementation uses the scipy.linalg implementation of the singular value decomposition.
    It only works for dense arrays and is not scalable to large dimensional data.

    """

    def __init__(self, n_components=None, copy=False):
        """ Create a new PCA model, with n_components components.

        - n_components should be > 0,
        - Use a SVD decomposition (scipy.linalg.eigh), and a pretty naive algorithm,
        - Minka's MLE strategy to automatically find the best choice of n_components is NOT YET implemented,
        - copy is NOT used yet.
        """
        self.n_components = n_components
        self.copy = copy
        # Trained attributes, not yet
        self._eig_vals = "None (Not yet trained)"
        self._eig_vecs = "None (Not yet trained)"
        self._energies = "None (Not yet trained)"
        self.rho_rapport = "None (Not yet trained)"
        self._projection = "None (Not yet trained)"

    def __str__(self):
        r""" str(self) -> str. """
        return "myPCA(copy={}, n_components={})\n- rho_rapport = {:.3%}".format(self.copy, self.n_components, self.rho_rapport)

    __repr__ = __str__

    def fit(self, X, rho=False):
        """ Train the PCA model on data X..

        - X should not be too big (5000 samples, 1000 features max),
        - Every call to fit() will ERASE the memory of the previous fits,
        - if rho is True, print the ratio of the k-cumulated spectral energy by the total spectral energy (should be close to 100% if you want the PCA to be meaningful).
        """
        k = self.n_components
        # n is the nb of samples, should stay the same
        # d is the dimension (nb of features), will be reduced to k
        # n, d = np.shape(X)
        # if d <= k:  # We are already reduced ?
        #     return X
        # 1. Computing the d-dimensional mean vector
        # mean_vector = np.mean(X, 0)
        # 2. Computing the d x d covariance matrix (no need for the mean_vector)
        cov_mat = np.cov(X.T)
        # 3. Computing eigenvectors and corresponding eigenvalues
        # cov_mat is symmetric, using eigh is quicker than using eig
        eig_val, eig_vec = eigh(cov_mat)
        # 4. Sorting the eigenvectors by decreasing eigenvalues
        # Note: eigh return the eigenvalues in ascending order, this could be avoided
        self._eig_vals = eig_val[::-1]
        self._eig_vecs = eig_vec[::-1]
        # # Make a list of (eigenvalue, eigenvector) tuples
        # eig_pairs = [(np.abs(eig_val[i]), eig_vec[:, i])
        #              for i in range(len(eig_val))]
        # # Sort the (eigenvalue, eigenvector) tuples from high to low
        # eig_pairs.sort()
        # eig_pairs.reverse()
        # eig_val = np.array([lbda for lbda, x in eig_pairs])
        # eig_vec = np.array([x for _, x in eig_pairs])
        # Compute cumulative energy
        self._energies = np.cumsum(self._eig_vals)
        self.rho_rapport = self._energies[k] / self._energies[-1]
        if rho:
            print("myPCA.fit(): k-cumulated spectral energy ratio (with k = {}) is {:g}.".format(k, self.rho_rapport))
        # 5. Choosing k eigenvectors with the largest eigenvalues
        self._projection = self._eig_vecs[0:k]
        # Done for the fitting part.

    def transform(self, X):
        """ Transform the data X (a numpy array-like) by the PCA transform. """
        # 6. Transforming the samples onto the new subspace
        transformed = np.dot(X, np.transpose(self._projection))
        return transformed

    def fit_transform(self, X):
        """ Fit the PCA model on X, and then transform the data X (a numpy array-like) by the PCA transform. """
        self.fit(X)
        return self.transform(X)

# End of pca.py
